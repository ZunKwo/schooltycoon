
#ifndef LOG_H_
# define LOG_H_

# include <iostream>
# include <fstream>
# include <string>

class		Log
{
public:
	enum	MessageType
	{
		Notified,
		Warning,
		Error
	};

public:
	Log(const std::string& fileName);
	virtual ~Log();

	void	logMessage(MessageType type, const std::string& message);

private:
	std::ofstream	_file;
};

#endif //!LOG_H_