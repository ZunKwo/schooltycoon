
#ifndef LOGMANAGER_H_
# define LOGMANAGER_H_

# include "Log.h"

# include <string>
# include <map>

class		LogManager
{
public:
	static LogManager*	getInstance();
	static void			destroy();

	Log*				getLog(const std::string& logName);

private:
	LogManager();
	~LogManager();

private:
	static LogManager*			_instance;

	std::map<std::string, Log*>	_logs;
};

#endif //!LOGMANAGER_H_