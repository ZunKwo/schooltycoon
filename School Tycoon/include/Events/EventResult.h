
#ifndef EVENTRESULT_H_
# define EVENTRESULT_H_

# include "Events/Keyboard.h"
# include "Events/Mouse.h"

/**
*	This class is a container for the result of I from the user (Keyboard / Mouse).
**/
class		EventResult
{
public:
	EventResult();
	virtual ~EventResult();

	/**
	*	Return the Keyboard class. You can set values for the Keyboard input.
	*	(see Keyboard.h for more details.)
	**/
	inline Keyboard&	getKeyboard();

	/**
	*	Return the Mouse class. You can set values for the Keyboard input.
	*	(see Mouse.h for more details.)
	**/
	inline Mouse&		getMouse();

private:
	Keyboard	_keyboard;
	Mouse		_mouse;
};

#endif //!EVENTRESULT_H_