
#ifndef EVENTINPUTS_H_
# define EVENTINPUTS_H_

namespace	EventInputs
{
	enum	Keyboard
	{
		Escape = 0
	};

	enum	Mouse
	{
		RightButton = 0,
		MiddleButton,
		LeftButton,
		WheelUp,
		WheelDown
	};
};

#endif //!EVENTINPUTS_H_