
#ifndef MOUSE_H_
# define MOUSE_H_

# include "EventInputs.h"

# include <map>

/**
*	Container for the Mouse inputs.
**/
class	Mouse
{
public:
	Mouse();
	virtual ~Mouse();

	/**
	*	You have to call this for each key pressed.
	*	It set the value of the key at true in the map.
	**/
	void									isPressed(EventInputs::Mouse key);
	/**
	*	You have to call this for each key released.
	*	It set the value of the key at false in the map.
	**/
	void									isReleased(EventInputs::Mouse key);

	/**
	*	Return the value of the key.
	**/
	inline bool								getKeyState(EventInputs::Mouse key) const;

	/**
	*	Set the value of the wheel.
	*	0 is when the wheel isn't moving.
	*	The others values depends on the library used.
	**/
	void									setWheelValue(float value);
	/**
	*	Get the wheel value.
	**/
	inline float							getWheelValue() const;

	/**
	*	Set the position of the mouse.
	**/
	void									setMousePosition(const std::pair<float, float>& position);
	/**
	*	Return the position of the mouse.
	**/
	inline const std::pair<float, float>&	getMousePosition() const;

private:
	std::map<EventInputs::Mouse, bool>		_mouse;
	float									_wheelValue;
	std::pair<float, float>					_mousePos;
};

#endif //!MOUSE_H_