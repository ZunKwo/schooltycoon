
#ifndef KEYBOARD_H_
# define KEYBOARD_H_

# include "EventInputs.h"

# include <map>

/**
*	Container for the Keyboard inputs.
**/
class	Keyboard
{
public:
	Keyboard();
	virtual ~Keyboard();

	/**
	*	You have to call this for each key pressed.
	*	It set the value of the key at true in the map.
	**/
	void									isPressed(EventInputs::Keyboard key);
	/**
	*	You have to call this for each key released.
	*	It set the value of the key at false in the map.
	**/
	void									isReleased(EventInputs::Keyboard key);

	/**
	*	Return the value of the key.
	**/
	inline bool								getKeyState(EventInputs::Keyboard key) const;

	/**
	*	Set the state of CTRL.
	**/
	void									setCtrlState(bool value);
	/**
	*	Get the state of CTRL.
	**/
	inline bool								getCtrlState() const;

	/**
	*	Set the state of ALT
	**/
	void									setAltState(bool value);
	/**
	*	Get the state of ALT
	**/
	inline bool								getAltState() const;

	/**
	*	Set the state of SHIFT
	**/
	void									setShiftState(bool value);
	/**
	*	Get the state of SHIFT
	**/
	inline bool								getShiftState() const;

private:
	std::map<EventInputs::Keyboard, bool>	_keyboard;
	bool									_ctrl;
	bool									_alt;
	bool									_shift;
};

#endif //!KEYBOARD_H_