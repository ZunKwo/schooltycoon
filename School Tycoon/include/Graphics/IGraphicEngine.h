
#ifndef IGRAPHICENGINE_H_
# define IGRAPHICENGINE_H_

# include "events/EventResult.h"

class IGraphicEngine
{
public:
	virtual bool		init() = 0;
	virtual void		inputUpdate(EventResult& result) = 0;
	virtual bool		frameUpdate() = 0;
};

#endif //!IGRAPHICENGINE_H_