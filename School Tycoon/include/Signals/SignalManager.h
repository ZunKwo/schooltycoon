
#ifndef SIGNALMANAGER_H_
# define SIGNALMANAGER_H_

# include "Signal.h"

# include <queue>

class		SignalManager
{
public:
	static SignalManager*	getInstance();
	static void				destroy();

	void					raise(Signal* signal);

private:
	SignalManager();
	~SignalManager();

private:
	static SignalManager*		_instance;
	std::queue<Signal*>			_queue;
};

#endif //!SIGNALMANAGER_H_