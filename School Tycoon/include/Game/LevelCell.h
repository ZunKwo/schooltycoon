
#ifndef LEVELCELL_H_
# define LEVELCELL_H_

# include <map>

class		LevelCell
{
public:
	enum	CellType
	{
		Constructible = 0,
		NotConstructible
	};

public:
	LevelCell();
	virtual ~LevelCell();

private:
	std::pair<std::size_t, std::size_t>	_position;
	CellType							_type;
};

#endif //!LEVELCELL_H_