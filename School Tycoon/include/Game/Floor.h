
#ifndef FLOOR_H_
# define FLOOR_H_

# include "Room.h"
# include "Corridor.h"
# include "Selection.h"

# include <map>

class	Floor
{
public:
	static Floor*	create(Selection* selection);
	void			destroy();

private:
	Floor();
	virtual ~Floor();

	void			createRoom(Selection* selection, std::size_t id);
	Room*			getRoom(std::size_t id);

	Corridor*		getCorridor();

private:
	std::map<std::size_t, Room*>	_rooms;
	Corridor*						_corridor;
};

#endif //!FLOOR_H_