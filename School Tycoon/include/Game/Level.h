
#ifndef LEVEL_H_
# define LEVEL_H_

# include "LevelCell.h"
# include "Building.h"
# include "Selection.h"

# include <map>
# include <vector>

class	Level
{
public:
	static Level*		create(const std::string& levelName);
	void				destroy();

private:
	Level();
	virtual ~Level();

public:
	void		createBuilding(Selection* selection, std::size_t id);
	Building*	getBuilding(std::size_t id);

private:
	std::vector<std::vector<LevelCell*> >	_map;
	std::map<std::size_t, Building*>		_buildings;
};

#endif //!LEVEL_H_