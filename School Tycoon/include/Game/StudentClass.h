#ifndef __STUDENTCLASS_H__
#define	__STUDENTCLASS_H__

#include <string>
#include <list>
#include "AI/StudentAI.h"

class	StudentClass
{
	enum classGrade
	{
		FIRST,
		SECOND,
		THIRD,
		FOURTH
	};

	std::string		_name;
	int				_id;
	int				_nbrStudent;
	classGrade		_grade;
	std::list<AAI*>	_studentList;

	//planning

public:
	StudentClass(std::string&, int, classGrade);
	~StudentClass();

	void	setName(std::string&);
	void	setStudentNbr(int);
	void	setGrade(classGrade);
	void	addStudent(AAI*);

	const	std::string&	getName() const;
	const	int				getStudentNbr() const;
	classGrade				getGrade();
	std::list<AAI*>			getStudent();
};

#endif