
#ifndef BUILDING_H_
# define BUILDING_H_

# include "Floor.h"
# include "Selection.h"

# include <string>
# include <vector>

class	Building
{
public:
	static Building*	create(Selection* selection, std::size_t id);
	void				destroy();

private:
	Building();
	virtual ~Building();

	void				setName(const std::string& name);
	const std::string&	getName() const;

	void				createFloor(Selection* selection);
	Floor*				getFloor(std::size_t floorNumber);

private:
	std::size_t			_id;
	std::string			_name;

	std::vector<Floor*>	_floors;
};

#endif //!BUILDING_H_