
#ifndef CORRIDOR_H_
# define CORRIDOR_H_

class	Corridor
{
public:
	static Corridor*	create(Selection* selection);
	void				destroy();

private:
	Corridor();
	virtual ~Corridor();

	void				deleteSelection(Selection* selection);
	void				addSelection(Selection* selection);

	void				freeCell(const std::pair<std::size_t, std::size_t>& position);
	void				setRoomDoor(const std::pair<std::size_t, std::size_t>& position, std::size_t roomId);
	void				setFloorStairs(const std::pair<std::size_t, std::size_t>& position, std::size_t floorNumber);
	void				setExit(const std::pair<std::size_t, std::size_t>& position, std::size_t exitNumber);

private:
	std::vector<std::vector<LevelCell*> >	_corridorMap;
};

#endif //!CORRIDOR_H_