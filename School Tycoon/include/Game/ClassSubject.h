#ifndef __CLASSSUBJECT_H__
#define __CLASSSUBJECT_H__

#include	<string>
#include	<map>
#include	<list>
#include	"AI/AAI.h"

typedef	std::map<std::pair<int, std::string>, std::list<AAI*> > subjectRet;

class	ClassSubject
{
private:
	subjectRet		_subjectList;


public:
	ClassSubject();
	~ClassSubject();

	void	addSubject(std::string&, std::list<AAI*>);
	void	addSubject(std::string&);
	void	addTeacherToSubject(int, AAI*);
	void	deleteTeacherFromSubject(int, int);
	void	deleteSubject(int);

	subjectRet&			getSubjectList();
	std::list<AAI*>&	getTeacherBySubject(int);


};

#endif