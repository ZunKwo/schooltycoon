
#ifndef SELECTION_H_
# define SELECTION_H_

class	Selection
{
public:
	Selection();
	virtual ~Selection();

	void	start(const std::pair<std::size_t, std::size_t>& position);
	void	end(const std::pair<std::size_t, std::size_t>& position);

	void	addCell(const std::pair<std::size_t, std::size_t>& position);
	void	clear();

private:
	std::vector<std::vector<std::size_t> >	_selection;
};

#endif //!SELECTION_H_