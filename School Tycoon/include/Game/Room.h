
#ifndef ROOM_H_
# define ROOM_H_

class	Room
{
public:
	static Room*	create(Selection* selection);
	void			destroy();

private:
	Room();
	virtual ~Room();

public:
	void			setEntrance(const std::pair<std::size_t, std::size_t>& position);

private:
	std::vector<std::vector<LevelCell*> >	_roomMap;
};

#endif //!ROOM_H_