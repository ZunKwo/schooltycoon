
#ifndef STATEDATA_H_
# define STATEDATA_H_

# include <cstdlib>

class	StateData
{
public:
	StateData();
	virtual ~StateData();

	void			setCurrentState(std::size_t state);
	std::size_t		getCurrentState() const;

private:
	std::size_t		_currentState;
};

#endif //STATEDATA_H_