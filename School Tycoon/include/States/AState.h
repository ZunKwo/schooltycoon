
#ifndef ASTATE_H_
# define ASTATE_H_

# include "events/EventResult.h"
# include "StateData.h"

class				AState
{
public:
	AState();
	virtual ~AState();

	virtual void	init() = 0;
	virtual void	update(StateData& data, const EventResult& result) = 0;
	virtual void	reset() = 0;

	void			treatData(StateData& data);

private:
	virtual void	useData(StateData& data) = 0;
};

#endif //ASTATE_H_