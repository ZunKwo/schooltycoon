
#ifndef GAMEAPPLICATION_H_
# define GAMEAPPLICATION_H_

# include "States/AState.h"
# include "States/StateData.h"
# include "Graphics/IGraphicEngine.h"
# include "Events/EventResult.h"

# include <map>

class								GameApplication
{
public:
	GameApplication(IGraphicEngine* graphicEngine);
	virtual ~GameApplication();

	void							go();

private:
	bool							eventsUpdate();
	bool							stateUpdate();
	bool							graphicUpdate();

private:
	IGraphicEngine*					_graphicEngine;
	EventResult						_eventResult;
	StateData						_stateData;

	std::size_t						_currentState;
	std::map<std::size_t, AState*>	_states;
};

#endif //!GAMEAPPLICATION_H_