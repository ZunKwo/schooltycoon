#ifndef __AICORE_H__
#define __AICORE_H__

#include	<map>
#include "AI/StudentAI.h"
#include "AI/TeacherAI.h"
#include "Game/StudentClass.h"

class StudentClass;

class	AICore
{

	std::map<AAI::AIType, std::list<AAI*> >		_aiList;
	std::list<StudentClass*>					_classList;

public:
	AICore();
	~AICore();


	void						createAI(std::string name, AICore&, AAI::AIType);
	void						deleteAI(int, AAI::AIType);

	int							getAISize(AAI::AIType);
	const	std::list<AAI*>&	getAIList(AAI::AIType);
	AAI*						getAIbyId(int, AAI::AIType);

	void						sendMsgTo(int, std::string&);

	void						clearAIList(AAI::AIType);

};

#endif