#ifndef __TEACHERAI_H__
#define __TEACHERAI_H__

#include "AI/AAI.h"
#include "AI/AICore.h"

class AICore;

class TeacherAI	: public AAI
{
	AICore&	_core;

public:
	TeacherAI(int, std::string, AICore&, AAI::AIType);
	~TeacherAI();

	virtual	void	speak(int, std::string&);
	virtual	void	receave(std::string&);
	virtual	AICore&	getAICore();
};

#endif