#ifndef __AAI_H__
#define __AAI_H__

#include <string>
#include <list>


class			AAI
{
public:
	enum MoveType
	{
		UP,
		DOWN,
		RIGHT,
		LEFT,
		NONE
	};
	enum AIType
	{
		STUDENT,
		TEACHER,
		SUPERVISOR,
		MAINTAINER
	};

private:
	int						_id;
	int						_posX;
	int						_posY;
	AIType					_type;
	std::string				_name;
	std::list<MoveType>		_moveList;

	//planning a ajouter
	//pathfinding class reference

public:
	virtual					~AAI();

	virtual	void			moveX(int);
	virtual	void			moveY(int);
	virtual	void			move();
	virtual	void			setX(int);
	virtual	void			setY(int);
	virtual	void			setId(int);
	virtual	void			setType(AIType);
	virtual	void			setName(std::string);
	virtual	void			clearMoveList();
	

	virtual	const	int&				getY() const;
	virtual	const	int&				getX() const;
	virtual	const	int&				getId() const;
	virtual	const	AIType				getType() const;
	virtual	const	std::string&		getName() const;
	virtual	MoveType					getNextMove();

	virtual	void			fillPathTo(size_t);

	virtual	void			speak(int, std::string&) = 0;
	virtual	void			receave(std::string&) = 0;
};


#endif