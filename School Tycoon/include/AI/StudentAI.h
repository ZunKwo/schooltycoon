#ifndef __STUDENTAI_H__
#define __STUDENTAI_H__

#include "AI/AAI.h"
#include "AI/AICore.h"
#include "Game/StudentClass.h"

class AICore;
class StudentClass;

class StudentAI	: public AAI
{
	int	_disciplineLevel;
	AICore&	_core;
	bool	_goNextClass;
	StudentClass*	_class;

public:

	StudentAI(int, std::string, AICore&, AAI::AIType);
	~StudentAI();

	virtual	void	speak(int, std::string&);
	virtual	void	receave(std::string&);
	virtual	bool	getGoClass();
	virtual	void	setGoClass(bool);
	virtual	void	setClass(StudentClass*);
	virtual	void	setDiscipline(int);
	virtual	void	addDiscipline(int);
	virtual	int		getDiscipline();
	StudentClass*	getClass();

	virtual	void	isGoingNextClass();

	virtual	AICore&	getAICore();
};

#endif