
#include "GameApplication.h"

#include <iostream>

int	main(int ac, char** av)
{
	IGraphicEngine*	graphicEngine = 0;

	GameApplication gameApplication(graphicEngine);

	gameApplication.go();

	return 0;
}