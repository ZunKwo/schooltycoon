#include "Events/Keyboard.h"

Keyboard::Keyboard()
{

}

Keyboard::~Keyboard()
{

}

void									Keyboard::isPressed(EventInputs::Keyboard key)
{
	_keyboard[key] = true;
}

void									Keyboard::isReleased(EventInputs::Keyboard key)
{
	_keyboard[key] = false;
}

bool									Keyboard::getKeyState(EventInputs::Keyboard key) const
{
	return _keyboard.at(key);
}

void									Keyboard::setCtrlState(bool value)
{
	_ctrl = value;
}

bool									Keyboard::getCtrlState() const
{
	return _ctrl;
}

void									Keyboard::setAltState(bool value)
{
	_alt = value;
}

bool									Keyboard::getAltState() const
{
	return _alt;
}

void									Keyboard::setShiftState(bool value)
{
	_shift = value;
}

bool									Keyboard::getShiftState() const
{
	return _shift;
}