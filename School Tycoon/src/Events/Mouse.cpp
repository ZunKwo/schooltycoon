#include "Events/Mouse.h"

Mouse::Mouse()
{
}

Mouse::~Mouse()
{
}

void									Mouse::isPressed(EventInputs::Mouse key)
{
	_mouse[key] = true;
}

void									Mouse::isReleased(EventInputs::Mouse key)
{
	_mouse[key] = false;
}

bool									Mouse::getKeyState(EventInputs::Mouse key) const
{
	return _mouse.at(key);
}

void									Mouse::setWheelValue(float value)
{
	_wheelValue = value;
}

float									Mouse::getWheelValue() const
{
	return _wheelValue;
}

void									Mouse::setMousePosition(const std::pair<float, float>& position)
{
	_mousePos = position;
}

const std::pair<float, float>&			Mouse::getMousePosition() const
{
	return _mousePos;
}