#include <ctime>
#include "AI/StudentAI.h"


StudentAI::StudentAI(int id, std::string name, AICore& core, AAI::AIType type) : _core(core)
{
	srand(time(NULL));
	setId(id);
	setType(type);
	setName(name);
	setX(0);
	setY(0);
	setDiscipline(rand() % 100);
	setGoClass(true);
	clearMoveList();
	setClass(NULL);
}


StudentAI::~StudentAI()
{

}

void	StudentAI::speak(int id, std::string& msg)
{
	AICore&	core = this->getAICore();

	core.sendMsgTo(id, msg);
}

void	StudentAI::receave(std::string& msg)
{

}

AICore&	StudentAI::getAICore()
{
	return(_core);
}

bool	StudentAI::getGoClass()
{
	return (_goNextClass);
}

void	StudentAI::setGoClass(bool next)
{
	_goNextClass = next;
	if (next == true)
		addDiscipline(1);
	else
		addDiscipline(-1);
}

void	StudentAI::setClass(StudentClass* nClass)
{
	_class = nClass;
}

void	StudentAI::setDiscipline(int disc)
{
	_disciplineLevel = disc;
}

void	StudentAI::addDiscipline(int disc)
{
	_disciplineLevel += disc;
	if (_disciplineLevel < 0)
		_disciplineLevel = 0;
	if (_disciplineLevel > 100)
		_disciplineLevel = 100;
}

int		StudentAI::getDiscipline()
{
	return(_disciplineLevel);
}

StudentClass*	StudentAI::getClass()
{
	return (_class);
}

void	StudentAI::isGoingNextClass()
{
	int	val;

	val = rand() % 100;

	if (val > getDiscipline())
	{
		val = rand() % 100;
		if (val > 33)
			setGoClass(false);
		else
			setGoClass(true);
	}
	else
	{
		setGoClass(true);
	}
}