#include "AI/TeacherAI.h"


TeacherAI::TeacherAI(int id, std::string name, AICore& core, AAI::AIType type) : _core(core)
{
	setId(id);
	setType(type);
	setName(name);
	setX(0);
	setY(0);
	clearMoveList();
}


TeacherAI::~TeacherAI()
{

}

void	TeacherAI::speak(int id, std::string& msg)
{
	AICore&	core = this->getAICore();

	core.sendMsgTo(id, msg);
}
void	TeacherAI::receave(std::string& msg)
{

}

AICore&	TeacherAI::getAICore()
{
	return(_core);
}