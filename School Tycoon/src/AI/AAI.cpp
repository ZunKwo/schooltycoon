#include "AI/AAI.h"

AAI::~AAI()
{

}

void	AAI::moveX(int x)
{
	_posX += x;
	//send signal
}

void	AAI::moveY(int y)
{
	_posY += y;
	//send signal
}

void	AAI::move()
{
	MoveType	next;

	next = getNextMove();
	if (next != NONE)
	{
		if (next == UP)
			moveX(-1);
		else if(next == DOWN)
			moveX(1);
		else if(next == LEFT)
			moveY(-1);
		else if(next == RIGHT)
			moveY(1);
	}

}

void	AAI::setX(int x)
{
	_posX = x;
}

void	AAI::setY(int y)
{
	_posY = y;
}

void	AAI::setId(int id)
{
	_id = id;
}

void	AAI::setType(AAI::AIType type)
{
	_type = type;
}


void	AAI::setName(std::string name)
{
	_name = name;
}

void	AAI::clearMoveList()
{
	_moveList.clear();
}

const	int&	AAI::getY() const
{
	return (_posY);
}

const	int&	AAI::getX() const
{
	return (_posX);
}

const	int&	AAI::getId() const
{
	return (_id);
}


const	AAI::AIType	AAI::getType() const
{
	return (_type);
}


const	std::string&	AAI::getName() const
{
	return (_name);
}

AAI::MoveType	AAI::getNextMove()
{
	MoveType ret;

	if (!(_moveList.empty()))
	{
		ret = _moveList.front();
		_moveList.pop_front();
		return (ret);
	}
	else
		return (MoveType::NONE);
}


void	AAI::fillPathTo(size_t roomId)
{
//TODO!
}
