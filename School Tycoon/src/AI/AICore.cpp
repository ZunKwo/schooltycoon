#include "AI/AICore.h"

AICore::AICore()
{
	_aiList.clear();
}

AICore::~AICore()
{
	AAI*	tmp;


	/*while (_aiList.size() > 0)
	{
		tmp = _studentList.front();
		_studentList.pop_front();
		delete (tmp);
	}*/

}



void	AICore::createAI(std::string name, AICore& core, AAI::AIType type)
{
	int	newId;
	AAI*	newAI;

	newId = getAISize(type) + 1;
	if (type == AAI::STUDENT)
		newAI = new StudentAI(newId, name, core, type);
	else if (type == AAI::TEACHER)
		newAI = new TeacherAI(newId, name, core, type);
	_aiList[type].push_back(newAI);
}

void	AICore::deleteAI(int id, AAI::AIType type)
{
	std::list<AAI*>::iterator	it;
	std::list<AAI*>::iterator	itEnd;

	itEnd = _aiList[type].end();
	it = _aiList[type].begin();
	while (it != itEnd && (*it)->getId() != id)
	{
		it++;
	}
	if (it != itEnd)
	{
		_aiList[type].erase(it);
	}
}

int		AICore::getAISize(AAI::AIType type)
{
	return (_aiList[type].size());
}

const	std::list<AAI*>&	AICore::getAIList(AAI::AIType type)
{
	return (_aiList[type]);
}

void	AICore::sendMsgTo(int id, std::string& msg)
{
	std::list<AAI*>::iterator	it;
	std::list<AAI*>::iterator	itEnd;


	itEnd = _aiList[AAI::STUDENT].end();
	it = _aiList[AAI::STUDENT].begin();

	while (it != itEnd && (*it)->getId() != id)
	{
		it++;
	}
	if (it != itEnd)
	{
		(*it)->receave(msg);
	}
}

AAI*	AICore::getAIbyId(int id, AAI::AIType type)
{
	std::list<AAI*>::const_iterator	it;
	std::list<AAI*>::const_iterator	itEnd;


	itEnd = _aiList[type].end();
	it = _aiList[type].begin();


	while (it != itEnd && (*it)->getId() != id)
	{
		it++;
	}
	if (it != itEnd)
	{
		return((*it));
	}
	return (NULL);
}


void	AICore::clearAIList(AAI::AIType type)
{
	_aiList[type].clear();
}