#include "GameApplication.h"
#include "Logs/LogManager.h"

GameApplication::GameApplication(IGraphicEngine* graphicEngine) :
	_graphicEngine(graphicEngine),
	_currentState(0)
{
}

GameApplication::~GameApplication()
{
}

void			GameApplication::go()
{
	if (!_graphicEngine)
	{
		LogManager::getInstance()->getLog("GameApplication.log")->logMessage(Log::MessageType::Error, "GameApplication->go() --- [GraphicEngine null]");
		return;
	}

	bool		running = _graphicEngine->init();

	while (running)
	{
		running = eventsUpdate();
		
		if (running)
			running = stateUpdate();

		if (running)
			running = graphicUpdate();
	}
}

bool			GameApplication::eventsUpdate()
{
	_graphicEngine->inputUpdate(_eventResult);
	return true;
}

bool			GameApplication::stateUpdate()
{
	if (_currentState == 0)
		return false;

	_states[_currentState]->update(_stateData, _eventResult);

	if (_stateData.getCurrentState() == 0)
		return false;

	if (_currentState != _stateData.getCurrentState())
	{
		_states[_currentState]->reset();
		_currentState = _stateData.getCurrentState();
		_states[_currentState]->init();
	}

	return true;
}

bool			GameApplication::graphicUpdate()
{
	_graphicEngine->frameUpdate();
	return true;
}