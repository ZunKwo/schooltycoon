#include <utility>
#include	"Game/ClassSubject.h"

ClassSubject::ClassSubject()
{
}

ClassSubject::~ClassSubject()
{
}

void	ClassSubject::addSubject(std::string& name)
{
	
	int	id;
	std::list<AAI*>	nullList;

	id = (_subjectList.end())->first.first;
	id += 1;
	std::pair<int, std::string&>	info(id, name);

	nullList.clear();
	std::pair<std::pair<int, std::string&>, std::list<AAI*> >	tmp(info, nullList);

	_subjectList.insert(tmp);
}

void	ClassSubject::addSubject(std::string& name, std::list<AAI*> teatcherList)
{
	int	id;

	id = (_subjectList.end())->first.first;
	id += 1;

	std::pair<int, std::string&>	info(id, name);
	std::pair<std::pair<int, std::string&>, std::list<AAI*> >	tmp(info, teatcherList);

	_subjectList.insert(tmp);
}

void	ClassSubject::addTeacherToSubject(int subjectId, AAI* teacher)
{
	subjectRet::iterator	it;
	subjectRet::iterator	itEnd;

	it = _subjectList.begin();
	itEnd = _subjectList.end();
	while ((*it).first.first != subjectId && it != itEnd)
		it++;
	if (it != itEnd)
	{
		(*it).second.push_back(teacher);
	}
}

void	ClassSubject::deleteTeacherFromSubject(int subjectId, int teacherId)
{
	subjectRet::iterator	it;
	subjectRet::iterator	itEnd;
	std::list<AAI*>::iterator	teacherIt;
	std::list<AAI*>::iterator	teacherItEnd;

	it = _subjectList.begin();
	itEnd = _subjectList.end();
	while ((*it).first.first != subjectId && it != itEnd)
		it++;
	if (it != itEnd)
	{
		teacherIt = (*it).second.begin();
		teacherItEnd = (*it).second.end();
		while (teacherIt != teacherItEnd && (*teacherIt)->getId() != teacherId)
		{
			teacherIt++;
		}
		if (teacherIt != teacherItEnd)
			(*it).second.erase(teacherIt);
	}
}

void	ClassSubject::deleteSubject(int subjectId)
{
	subjectRet::iterator	it;
	subjectRet::iterator	itEnd;

	it = _subjectList.begin();
	itEnd = _subjectList.end();
	while ((*it).first.first != subjectId && it != itEnd)
		it++;
	if (it != itEnd)
	{
		_subjectList.erase(it);
	}
}

subjectRet&		ClassSubject::getSubjectList()
{
	return (_subjectList);
}

std::list<AAI*>&	ClassSubject::getTeacherBySubject(int idSubject)
{
	subjectRet::iterator	it;
	subjectRet::iterator	itEnd;

	it = _subjectList.begin();
	itEnd = _subjectList.end();
	while ((*it).first.first != idSubject && it != itEnd)
		it++;
	if (it != itEnd)
	{
		return((*it).second);
	}
}