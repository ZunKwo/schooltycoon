#include	"Game/StudentClass.h"

StudentClass::StudentClass(std::string& name, int nbr, classGrade grade)
{
	_name = name;
	_nbrStudent = nbr;
	_grade = grade;
}

StudentClass::~StudentClass()
{
}

void	StudentClass::setName(std::string& name)
{
	_name = name;
}

void	StudentClass::setStudentNbr(int nbr)
{
	_nbrStudent = nbr;
}

void	StudentClass::setGrade(classGrade grade)
{
	_grade = grade;
}

void	StudentClass::addStudent(AAI* student)
{
	_studentList.push_back(student);
}

const	std::string&	StudentClass::getName() const
{
	return (_name);
}

const	int				StudentClass::getStudentNbr() const
{
	return (_nbrStudent);
}

StudentClass::classGrade		StudentClass::getGrade()
{
	return (_grade);
}

std::list<AAI*>			StudentClass::getStudent()
{
	return (_studentList);
}

