#include "Logs/LogManager.h"

LogManager*	LogManager::_instance = 0;

LogManager*	LogManager::getInstance()
{
	if (!_instance)
		_instance = new LogManager();
	return _instance;
}

void		LogManager::destroy()
{
	if (_instance)
		delete _instance;
}

Log*				LogManager::getLog(const std::string& logName)
{
	if (_logs.find(logName) == _logs.end())
		_logs[logName] = new Log(logName);

	return _logs[logName];
}

LogManager::LogManager()
{

}

LogManager::~LogManager()
{

}