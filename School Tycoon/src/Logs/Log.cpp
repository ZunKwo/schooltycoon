#include "Logs/Log.h"

#include <ctime>

Log::Log(const std::string& fileName)
{
	_file.open(fileName, (std::ios::trunc | std::ios::out));
}

Log::~Log()
{
	_file.close();
}

void	Log::logMessage(MessageType type, const std::string& message)
{
	if (_file.is_open())
	{
		std::time_t timev = time(0);
		struct std::tm * now = localtime(&timev);

		_file << "[" << (now->tm_hour>9?"":"0")  << now->tm_hour << ":" << (now->tm_min>9?"":"0") << now->tm_min << ":" << (now->tm_sec>9?"":"0") << now->tm_sec << "]";
		_file << "[" << (type==Error?"ERROR":(type==Warning?"WARNING":"MESSAGE")) << "] : " << message << '\n'; 
	}
}