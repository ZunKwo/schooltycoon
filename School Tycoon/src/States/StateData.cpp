#include "States/StateData.h"

StateData::StateData() :
	_currentState(0)
{
}

StateData::~StateData()
{

}

void			StateData::setCurrentState(std::size_t state)
{
	_currentState = state;
}

std::size_t		StateData::getCurrentState() const
{
	return _currentState;
}